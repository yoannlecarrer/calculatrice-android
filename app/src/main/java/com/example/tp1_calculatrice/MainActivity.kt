package com.example.tp1_calculatrice

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.TextView
//import android.content.Context
//import android.content.ClipData
//import android.content.ClipboardManager

class MainActivity : AppCompatActivity() {

    lateinit var txtView: TextView;
    var isNewOperation = true
    var oldNumber = ""
    var operateur = ""
    //var clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        txtView = findViewById(R.id.txtInput)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        var number = txtView.text.toString()
        outState.putString("saveRes", number)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        val resString = savedInstanceState.getString("saveRes","")
        oldNumber=resString
        txtView.text = oldNumber

    }

    fun calcul() {
        var newNumber:String = txtView.text.toString()
        var res = 0.0
        when (operateur) {
            "+" -> {
                res = oldNumber.toDouble() + newNumber.toDouble()
            }
            "-" -> {
                res = oldNumber.toDouble() - newNumber.toDouble()
            }
            "/" -> {
                res = oldNumber.toDouble() / newNumber.toDouble()
            }
            "x" -> {
                res = oldNumber.toDouble() * newNumber.toDouble()
            }
            "%" -> {
                res = oldNumber.toDouble() % newNumber.toDouble()
            }
            "" -> {
                res = newNumber.toDouble()
            }
        }
        txtView.text = res.toString()
    }

    fun onClickNumber(view: View) {
        if(isNewOperation) {
            txtView.text = ""
        }
        txtView.append((view as Button).text)
        isNewOperation = false
    }

    fun onClickClear(view: View) {
        isNewOperation = true
        txtView.text = ""
        operateur = ""
    }

    fun onClickDelete(view: View) {
        if (txtView.length() > 0) {
            var stringTxtInput = txtView.text.toString()
            var res = stringTxtInput.substring(0, txtView.length() - 1)
            this.txtView.text = res
        }
    }

    fun onClickNegation(view: View) {
        txtView.text = "-${txtView.text}"
    }

    fun onClickAdd(view: View) {
        isNewOperation = true
        if(txtView.text.isNotEmpty()) {
            if (operateur.isNotEmpty()) {
                calcul()
                oldNumber = txtView.text.toString()
                txtView.text = oldNumber
            } else {
                oldNumber = txtView.text.toString()
                txtView.text = ""
            }
            operateur = "+"
        }else{
            operateur = ""
        }
    }

    fun onClickSubstract(view: View) {
        isNewOperation = true
        if(txtView.text.isNotEmpty()) {
            if (operateur.isNotEmpty()) {
                calcul()
                oldNumber = txtView.text.toString()
                txtView.text = oldNumber
            } else {
                oldNumber = txtView.text.toString()
                txtView.text = ""
            }
            operateur = "-"
        }else{
            operateur = ""
        }
   }
   fun onClickMultiplication(view: View) {
        isNewOperation = true
       if(txtView.text.isNotEmpty()) {
           if (operateur.isNotEmpty()) {
               calcul()
               oldNumber = txtView.text.toString()
               txtView.text = oldNumber
           } else {
               oldNumber = txtView.text.toString()
               txtView.text = ""
           }
           operateur = "x"
       }else{
           operateur = ""
       }
   }

   fun onClickDivision(view: View) {
        isNewOperation = true
       if(txtView.text.isNotEmpty()) {
           if (operateur.isNotEmpty()) {
               calcul()
               oldNumber = txtView.text.toString()
               txtView.text = oldNumber
           } else {
               oldNumber = txtView.text.toString()
               txtView.text = ""
           }
           operateur = "/"
       }else{
           operateur = ""
       }
   }

    fun onClickModulo(view: View) {
        isNewOperation = true
        if(txtView.text.isNotEmpty()) {
            if (operateur.isNotEmpty()) {
                calcul()
                oldNumber = txtView.text.toString()
                txtView.text = oldNumber
            } else {
                oldNumber = txtView.text.toString()
                txtView.text = ""
            }
            operateur = "%"
        }else{
            operateur = ""
        }
    }

    fun onClickEquals(view: View) {
        calcul()
        operateur = ""
        isNewOperation = true
    }



    //fun onClickCTRLC(view: View) {
          // val textToCopy = txtView.text
        //val clip: ClipData = ClipData.newPlainText("simple text",textToCopy)
        //clipboard.primaryClip = clip
    //}
}





