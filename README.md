# Calculatrice Android

Construire une calculatrice avec les boutons suivants :
- une ligne en haut pour les valeurs saisies,
- 10 boutons pour les chiffres de 0 à 9,
- 5 boutons pour les 5 opérations arithmétiques de base sur les entiers : addition, soustraction, multiplication, division et modulo,
- un bouton pour la négation,
- un bouton qui permet d’évaluer l’expression en cours,
- un bouton pour effacer le dernier caractère (chiffre ou opération),
- un bouton pour tout effacer,
- un bouton pour les nombres à virgule (qu’il n’est pas nécessaire d’implémenter pour ce TP).


Les fonctionnalités :
- Il n’est pas possible d’entrer des valeurs directement sur la ligne du haut sans cliquer sur les boutons.
- Le clic sur un chiffre l’ajoute à la fin de la ligne du haut.
- Le clic sur une opération l’ajoute à la fin de la ligne du haut. Si la ligne contient déjà une opération, cette opération est effectuée et son résultat affiché avant cet affichage d’opération.
- Le clic sur le bouton “=” effectue l’opération choisie et affiche le résultat, quand il y a une opération, sinon rien n’est modifié.
- Le clic sur le bouton de négation (moins unaire) remplace le dernier nombre saisi par son opposé.
- Le clic sur le bouton “effacer” efface le dernier caractère que ce soit un chiffre ou une opération.
Effacement du dernier chiffre ne doit bien sûr pas afficher un zéro. (Pour les nombres négatifs, on efface le signe “moins” en même temps que le seul chiffre d’un nombre à un chiffre).
- Le clic sur le bouton “reset” efface toute la ligne du haut.
- La rotation de l’écran garde ce qui était saisi avant la rotation.
- Il est possible de copier le résultat du calcul dans le presse–papier.
- La mise en page est différente en mode portrait et en mode paysage.

